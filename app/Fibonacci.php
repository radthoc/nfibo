<?php

namespace App;

class Fibonacci implements FibonacciInterface
{
    private $series = [0, 1];

    /**
     * @param int $number
     * @return int
     */
    public function getNumber(int $number): int
    {
        if ($number == 0 || $number == 1) {
            return $number;
        }

        $this->getSerieTillNumber($number);

        return $this->series[$number - 1];
    }

    private function getSerieTillNumber(int $number)
    {
        for ($ix = 2; $ix < $number; $ix++) {
            $this->series[$ix] = $this->series[$ix - 1] + $this->series[$ix - 2];
        }
    }
}
