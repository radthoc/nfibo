<?php

namespace Tests\Unit;

use App\Fibonacci;
use Tests\TestCase;

class FibonacciTest extends TestCase
{
    /**
     * @dataProvider NumbersProvider
     *
     * @param int $number
     * @param int $expectedNumber
     */
    public function testGetNumber(int $number, int $expectedNumber)
    {
        $fibonacci = new Fibonacci();
        $this->assertEquals($expectedNumber, $fibonacci->getNumber($number));
    }

    public function NumbersProvider()
    {
        return [
            [8, 13],
            [6, 5],
            [12, 89],
            [16, 610],
            [22, 10946],
            [29, 317811],
        ];
    }
}
